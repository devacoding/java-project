package learnComparator;

import java.util.ArrayList;
import java.util.Collections;

public class Demo {

	public static void main(String[] args) {
		ArrayList names = new ArrayList();
		names.add("kannan");
		names.add("Deva");
		names.add("iyappan");
		names.add("venkatesh");
		names.add("ramachandran");
		System.out.println(names);  // Ascending order
		
		Collections.sort(names);
		System.out.println(names);
		ComparatorDemo cd = new ComparatorDemo();
		Collections.sort(names,cd);  // Descending order
		System.out.println(names);
		
		
	}

}
