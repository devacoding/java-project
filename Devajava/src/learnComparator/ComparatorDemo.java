package learnComparator;

import java.util.Comparator;

public class ComparatorDemo implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		String name1=(String)o1;
		String name2=(String)o2;
		
		if(name1.length()>name2.length())
			return -1;
		else if(name1.length()<name2.length())
			return +1;
		return 0;
	}
	

}
