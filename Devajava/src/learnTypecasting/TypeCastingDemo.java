package learnTypecasting;

public class TypeCastingDemo {

	public static void main(String[] args) {
		float price = 102.95f;
		int rev_price = (int) price;
		System.out.println(rev_price);    //privimitive Datatypes
		
		
		//char Name ="D";
		//String ch ="ff";

		
		char ch ='a';    //Asci value
		int ch2 =ch;
		System.out.println(ch2);
		
		
		char ch1 ='b';
		int ch3 = ch1;
		System.out.println(ch3);
		
		char d ='c';
		int e = d;
		System.out.println(e);
	}

}
