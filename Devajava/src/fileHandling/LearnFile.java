package fileHandling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LearnFile {

	public static void main(String[] args) throws IOException {
		File ff = new File("/home/devaraj/Pictures/raja/java1.txt");
		try {
		System.out.println(ff.createNewFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(ff.canWrite()) {
			FileWriter pen = new FileWriter(ff,true);
			//pen.write('1');
			//pen.write('2');
			//pen.write('3');
			//pen.write('4');
			//pen.flush();
			//pen.close();
			
			BufferedWriter bw = new BufferedWriter(pen);
			bw.write("Deva");
			bw.write("Raj");
			bw.write("King");
			bw.write("Raja");
			bw.flush();
			bw.close();
			
			
		}
		if(ff.canRead()) {
			FileReader reader = new FileReader(ff);
			BufferedReader bw = new BufferedReader(reader);
			//int ch= reader.read();
			String result = bw.readLine();
		//	while(ch!=-1) {
			while(result!=null) {
				
				//System.out.println((char)ch);
				//ch=reader.read();
				System.out.println(result);
				result=bw.readLine();
			}
		}

	}

}
