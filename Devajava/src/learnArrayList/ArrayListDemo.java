package learnArrayList;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListDemo {

	public static void main(String[] args) {
		
		ArrayList al = new ArrayList();
		al.add("deva");
		al.add("king");
		al.add(10);
		al.add(10.5f);
		al.add(true);
		System.out.println(al);
		al.add(2,"abc");
		System.out.println(al);
		System.out.println(al.contains(10.5f));
		System.out.println(al.indexOf("xyz"));
		System.out.println(al.isEmpty());
		al.add("deva");
		System.out.println(al.lastIndexOf("deva"));
		System.out.println(al);
		System.out.println(al.indexOf("deva"));
		al.remove(6);
		System.out.println(al);
		al.remove("abc");
		System.out.println(al);
		Object[] ob = al.toArray();
		for(int i=0;i<ob.length;i++)
			System.out.println(ob[i]);
		ArrayList al2 = new ArrayList();
		al2.addAll(al);
		System.out.println("al2 containers=>"+al2);
		al2.add("singam");
		al2.add("raj");
		
		
		
		
		ArrayList names = new ArrayList();
		names.add("ventakesh");
		names.add("Deva");
		names.add("gugan");
		names.add("selva");
		System.out.println(names);
		Collections.sort(names);
		System.out.println(names);
		
		
		
		
		
		
	}

}
